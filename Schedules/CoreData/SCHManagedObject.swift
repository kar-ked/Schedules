//
//  SCHManagedObject.swift
//  Schedules
//
//  Created by Karol Kędziora on 10.03.2016.
//  Copyright © 2016 Karol Kędziora. All rights reserved.
//

import Foundation
import CoreData

class SCHManagedObject: NSManagedObject {
    class func entityName() {
        return NSStringFromClass(self)
    }

    class func newInstanceInManagedObjectContext(context: NSManagedObjectContext) {
        let object = NSEntityDescription.insertNewObjectForEntityForName(self.entityName(), inManagedObjectContext: context)

        return object
    }

    class func fetchRequest() {
        return NSFetchRequest.fetchRequestWithEntityName(self.entityName())
    }

    class func allInstancesWithPredicate(predicate: NSPredicate, context: NSManagedObjectContext) {
        let request: NSFetchRequest = self.fetchRequest()
        request.predicate = predicate
        do {
            let results = try context.executeFetchRequest(request)
        }catch {
            print("ERROR loading \(predicate): \(error)")
        }
        return results
    }

    class func allInstancesInManagedObjectContext(context: NSManagedObjectContext) {
        let results = self.allInstancesWithPredicate(nil, context: context)
        return results
    }
}