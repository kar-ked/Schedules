//
//  ArrivalMO+CoreDataProperties.swift
//  Schedules
//
//  Created by Karol Kędziora on 09.03.2016.
//  Copyright © 2016 Karol Kędziora. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ArrivalMO {

    @NSManaged var marks: String?
    @NSManaged var busName: String?
    @NSManaged var time: String?
    @NSManaged var schedule: ScheduleMO?

}
