//
//  ParseHelper.swift
//  Schedules
//
//  Created by Karol Kędziora on 09.03.2016.
//  Copyright © 2016 Karol Kędziora. All rights reserved.
//

import Foundation
import SwiftyJSON

class ParseHelper {
    func parseBusStopsListFromJSON(data: AnyObject?) -> [BusStop]? {
        let json = JSON(data!)
        if let jsonBusStops = json["busStopsList"].array {
            var busStopsList = [BusStop]()
            for jsonBusStop in jsonBusStops {
                let busStop = BusStop()
                busStop.id = jsonBusStop["id"].int!
                busStop.name = jsonBusStop["name"].string!

                busStopsList.append(busStop)
            }
            return busStopsList
        }
        return nil
    }

    func parseSchedulesListFromJSON(data: AnyObject?) -> (Dictionary<String, String>?, [Schedule]?)? {
        let json = JSON(data!)

        let legend = json["legend"].dictionaryObject as! Dictionary<String, String>

        if let jsonSchedules = json["schedules"].array {
            var schedulesList = [Schedule]()
            for jsonSchedule in jsonSchedules {
                let schedule = Schedule()
                schedule.initialBusStop = jsonSchedule["initialBusStop"].string!
                schedule.destinationBusStop = jsonSchedule["destinationBusStop"].string!
                if let jsonArrivals = jsonSchedule["arrivals"].array {
                    var arrivalsList = [Arrival]()
                    for jsonArrival in jsonArrivals {
                        let arrival = Arrival()
                        arrival.busName = jsonArrival["bus_name"].string!
                        arrival.time = String(jsonArrival["time"].string!.characters.dropLast(3))
                        arrival.marks = jsonArrival["marks"].arrayObject as! [String]

                        arrivalsList.append(arrival)
                    }
                    schedule.arrivals = arrivalsList
                }
                schedulesList.append(schedule)
            }
            return (legend, schedulesList)
        }
        return nil
    }
}
