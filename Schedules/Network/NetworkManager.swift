//
//  NetworkManager.swift
//  Schedules
//
//  Created by Karol Kędziora on 09.03.2016.
//  Copyright © 2016 Karol Kędziora. All rights reserved.
//

import Foundation
import Alamofire

//let kBaseURL = "http://192.168.1.27/api/"
//let kBaseURL = "http://127.0.0.1/api/"
//let kBaseURL = "http://ugryzjablko.pl:6969/api/"
let kBaseURL = "http://yologurt.ddns.net/api/"

class NetworkManager {
    private let parser: ParseHelper = ParseHelper()
    static let sharedInstance = NetworkManager()

    private init() {}

    func fetchBusStops(callback: ([BusStop]?, NSError?) -> Void) {
        let url = kBaseURL + "bus_stops_list"

        Alamofire.request(.GET, url).responseJSON { response in
            switch response.result {
            case .Success(let data):
                if let busStopsList = self.parser.parseBusStopsListFromJSON(data){
                    callback(busStopsList, nil)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                callback(nil, error)
            }
        }
    }

    func fetchSchedulesForBusStop(busStopID: Int, callback: ((Dictionary<String, String>?, [Schedule]?)?, NSError?) -> Void) {
        let url = kBaseURL + "schedules/\(busStopID)"

        Alamofire.request(.GET, url).responseJSON { response in
            switch response.result {
            case .Success(let data):
                if let response = self.parser.parseSchedulesListFromJSON(data) {
                    callback(response, nil)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                callback(nil, error)
            }
        }
    }
}